require('dotenv').config();
const express = require("express");
const bodyParser = require("body-parser");
const request = require('request');
const sgMail = require('@sendgrid/mail');
const Joi = require('joi');
const port = process.env.PORT || 3000;
const compression = require('compression');

const app = express();

app.use(compression());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(__dirname));

app.get('/', function (req, res) {
    res.sendFile(__dirname + 'index');
});

app.post('/submit', function (req, res) {

    const schema = Joi.object().keys({
        name: Joi.string().alphanum().min(3).max(30).required(),
        subject: Joi.string().alphanum().min(3).max(30),
        bodyMessage: Joi.string().required(),
        email: Joi.string().email()
    });

    const resultJoi = Joi.validate(req.body, schema);

    if (req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
        return res.json({"responseCode": 1, "responseDesc": "Please select reCAPTCHA"});
    }
    const secretKey = process.env.RECAPTCHA_SECRET;
    const verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
    request(verificationUrl, function (error, response, body) {
        body = JSON.parse(body);
        if (body.success !== undefined && !body.success) {
            return res.json({"responseCode": 1, "responseDesc": "Failed captcha verification"});
        }
        else if (resultJoi && verificationUrl) {
            const {name, email, subject, bodyMessage} = req.body;

            sgMail.setApiKey(process.env.SENDGRID_API_KEY);

            const msg = {
                to: process.env.EMAIL_TO_1,
                //cc: process.env.EMAIL_TO_2,
                from: email,
                subject: subject,
                text: "My name is " + name + "." + " " + bodyMessage,
                html: "My name is " + name + "." + "<br>" + "<br>" + bodyMessage,
            };
            sgMail.send(msg);
            return res.json({"responseCode": 0, "responseDesc": "success !"});
        }
    });

});

app.use("*", function (req, res) {
    res.status(404).send("404");
});

app.listen(port);